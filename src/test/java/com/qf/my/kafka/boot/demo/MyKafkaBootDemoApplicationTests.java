package com.qf.my.kafka.boot.demo;

import com.alibaba.fastjson.JSON;
import com.qf.my.kafka.boot.demo.entity.DeviceDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@SpringBootTest
class MyKafkaBootDemoApplicationTests {

  @Autowired
  private KafkaTemplate<Integer,String> kafkaTemplate;

  @Autowired
  private RedisTemplate redisTemplate;

  @Test
  void testSend(){
    //用于存放30万台设备的设备key
    List<String> deviceKeyList = new ArrayList<>();
    //随机生成30万台设备的设备key
    for (int i = 0; i < 300000; i++) {
      String deviceKey = UUID.randomUUID().toString().replaceAll("-", "");
      deviceKeyList.add(deviceKey);
    }
    //使用线程池
    ExecutorService pool = Executors.newFixedThreadPool(8);
    while(true){
      pool.submit(new Runnable() {
        @Override
        public void run() {
          int index = new Random().nextInt(deviceKeyList.size());
          String deviceKey = deviceKeyList.get(index);
          if(!redisTemplate.hasKey(deviceKey)){
            //存入redis，并设置2秒有效期--用于模拟设备每隔2秒发一次心跳
            redisTemplate.opsForValue().set(deviceKey,"",2, TimeUnit.SECONDS);
            //向kafka发送消息
            DeviceDTO deviceDTO = new DeviceDTO(deviceKey,new Date().getTime());
            String json = JSON.toJSONString(deviceDTO);
            kafkaTemplate.send("my-replicated-topic",deviceDTO.getDeviceKey().hashCode(),json);
          }
        }
      });

    }

  }

  @Test
  void contextLoads() {


    for (int i = 0; i < 1000000000; i++) {

      DeviceDTO deviceDTO = new DeviceDTO(i+"",new Date().getTime());
      String json = JSON.toJSONString(deviceDTO);
      kafkaTemplate.send("my-replicated-topic",Integer.parseInt(deviceDTO.getDeviceKey()),json);
    }

  }




}
