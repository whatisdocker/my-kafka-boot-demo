package com.qf.my.kafka.boot.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

@Configuration
public class KafkaExecutorConfig {

  /**
   * 注入线程池对象
   * @return
   */
  @Bean(name = "listenerTaskExecutor")
  public ThreadPoolTaskExecutor listenerTaskExecutor() {
    ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
    //初始化线程数
    executor.setCorePoolSize(4);
    //最大线程数
    executor.setMaxPoolSize(8);
    executor.setQueueCapacity(100);
    executor.setThreadNamePrefix("my-consumer-pre-");
    //拒绝策略
    executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
    executor.setWaitForTasksToCompleteOnShutdown(true);
    executor.initialize();
    return executor;
  }


}
